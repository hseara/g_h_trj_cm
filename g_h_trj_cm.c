/*
 *                This source code is part of
 * 
 *                 G   R   O   M   A   C   S
 * 
 *          GROningen MAchine for Chemical Simulations
 * 
 *                        VERSION 3.0
 * 
 *          Description:
 *          The program takes a normal trajectory and give back  a 
 *          trajectory of the center of mass of the given fragments 
 *          
 */

/* This line is only for CVS version info */
static char *SRCID_template_c = "$Id: template.c,v 1.4 2001/07/23 15:28:29 lindahl Exp $";

#include <string.h>

#include <gromacs/statutil.h>
#include <gromacs/typedefs.h>
#include <gromacs/smalloc.h>
#include <gromacs/vec.h>
#include <gromacs/copyrite.h>
#include <gromacs/tpxio.h>
#include <gromacs/index.h>
#include <gromacs/confio.h>


/************************************************************************/
/* Print name of first atom in all groups in index file,Debug outpu only*/
static void print_types(atom_id index[], atom_id a[], int ngrps,
                        char *groups[], t_topology *top)
{
  int i;

  fprintf(stderr,"Using following groups: \n");
  for(i = 0; i < ngrps; i++)
    fprintf(stderr,"Groupname: %s First atomname: %s First atomnr %u (index=%u)\n",
            groups[i], *(top->atoms.atomname[a[index[i]]]), a[index[i]], index[i]);
  for(i = 0; i < ngrps; i++)
    fprintf(stderr,"idex[%d]=%u correspond a[index[%d]]=%u\n",
            i, index[i],i,a[index[i]]);
     
  fprintf(stderr,"\n");
}

/*************************************************************************/
/* Calcul centre masses fragment*/
static void CM_fragment(int isize_cm ,atom_id* index_cm,t_topology top,
                        t_trxframe fr,rvec rCM)
{
  int        i,ix;
  double     total_mass; 
  rvec       ri;
  /* necesita vector index_cm[i] que conte els numeros dels atoms del fragment 
  a calcular el CM  i el isize_cm que incluo el numero d'elements en index_c[i] */

    total_mass=0;
    clear_rvec(rCM);
    for(i=0;(i < isize_cm);i++){
      ix= index_cm[i];
       
      svmul(top.atoms.atom[ix].m,fr.x[ix],ri) ;
/*
      printf("Atom=%i,x=%8.5f,y=%8.5f,z=%8.5f\n ",
             ix,fr.x[ix][0],fr.x[ix][1],fr.x[ix][2]);
      printf("Atom=%i,massa=%8.5f,x=%8.5f,y=%8.5f,z=%8.5f\n ",
             ix,top.atoms.atom[ix].m,ri[0],ri[1],ri[2]);
*/
      rvec_inc(rCM,ri);
      total_mass=total_mass+top.atoms.atom[ix].m;
     }
     for ( i=0; (i<DIM); i++) rCM[i]=rCM[i]/total_mass;
/* 
     printf("\nCoordinates CM of Mass %8.5f at t=%8.3f : %8.5f %8.5f %8.5f\n",
             total_mass,fr.time,rCM[XX],rCM[YY],rCM[ZZ]);
*/
}

/*********************************************************************/

int main(int argc,char *argv[])
{

    const char *desc[] = {
     "Description:\n",
     "The program takes a normal trajectory and give back  a ", 
     "trajectory of the center of mass of the given fragments.\n", 
     "Options:\n",
     "-n argument takes the ndx files containing the fragments. ",
     "Every file is consider to contain a different kind of ",
     "fragment for output gro file.\n\n",
     "-o argument can only be a \"GRO\" file!!!!!\n"
    };
    #define NDESC asize(desc)

    static int   ndec=3;

    /*###############################################################
    Extra arguments - but note how you always get the begin/end
    options when running the program, without mentioning them here!
    ################################################################*/
    
    t_pargs pa[] = {
        { "-ndec", FALSE, etINT,
           { &ndec },
           "Precision for .gro writing in number of "
           "decimal places" }
    };
    #define NPA asize(pa)

    /*##############################################
    VARIABLES
    ################################################*/
    // General variables
    int          i,j,k;
    int          count;

    // Flags  
    gmx_bool     bDebug=FALSE;
    gmx_bool     bNeedPrec;
    gmx_bool     bSetPrec;
    gmx_bool     bHaveFirstFrame;

    // Parsing variables
    output_env_t oenv;

    // Reading topology
    char         top_title[256];
    t_topology   top;
    int          ePBC=-1;
    matrix       box;
  
    // Reading index definition CM
    char         **ndxinfiles;       // array to the name of index files         
    int          n_index_files=0;    // nr.index files with different fragments  
    t_blocka     **blocka;           // Data from index file
    char         **grpname;          // Groupnames   
    int          **n_at_grps=NULL;   // nr. of atoms inside each group
    int          ntotalgrps=0;       // nr. of total groups in the ndx files 

    // Output file
    const char   *out_file=NULL;/*Output file name                          */
    int          ftp;
    FILE         *out=NULL;
    real         prec;          /* precission of the output file = 10**ndec */
    t_trxframe   frout;         /* output trxframe struct                   */  
    t_atoms      CMatoms;       /* For the gro file. Stucture of CMatoms.   */

    
    // Reading trajectory
    const char   *in_file=NULL; /*Input trajectory file name*/
    t_trxstatus  *status;
    t_trxframe   fr;
   
    // Loop over frames
    rvec       ri,rCM,*xmem=NULL;
    int        isize_cm=0;         /* num atoms fragment per calcul CM         */
    char       * atom_type;
    char       title[STRLEN];
    atom_id    *index_cm=NULL;     /* index dels atoms pertanyents al fragment */
     


//  int        nndxin; 
//  char       **grpname;          /* groupnames                               */
//  t_block    **block;             /* data from index file                     */
//  int        *ngrps;             /* nr. of groups in index file              */
//  int        **n_at_grps=NULL;    /* nr. of atoms inside each group           */
//  int        *nra;                /* The number of atoms                      */
//  int        n_index_files=0;    /* nr.index files with different fragments  */   
//  real       prec;
 // t_atoms    new_atoms,useatoms;
//  char       pointer[4];
//  int        n_fr_tot_out=0;

    t_filenm fnm[] = {
     { efTPS,  NULL,      NULL, ffREAD  },/* this is for the topology */
     { efTRX, "-f" ,      NULL, ffREAD  },/* this for the trajectory */
     { efTRX, "-o" , "trajout", ffWRITE },/*this is for the output file*/
     { efNDX, "-n" ,"CM_index", ffRDMULT} /* and this for the indexes with fragments*/
    };
    #define NFILE asize(fnm)

    CopyRight(stderr,argv[0]);

    /*####################################################################
     * This is the routine responsible for adding default options,
     * calling the X/motif interface, etc. 
     *####################################################################*/
    parse_common_args(&argc,argv,PCA_CAN_TIME | PCA_CAN_VIEW,
		    NFILE,fnm,NPA,pa,NDESC,desc,0,NULL,&oenv);


    /*####################################################################
     * Command line options
     *####################################################################*/

    /* mark active cmdline options */
    bSetPrec  = opt2parg_bSet("-ndec", NPA, pa);

    /* ndec is in nr of decimal places, prec is a multiplication factor: */
    prec = 1;
    for (i=0; i<ndec; i++)
        prec *= 10;


    /*####################################################################
     * Reading topology file 
     *####################################################################*/ 
    read_tps_conf(ftp2fn(efTPS,NFILE,fnm),top_title,&top,&ePBC,NULL,NULL,box,TRUE);


    /*####################################################################
     * Reading index files 
     *####################################################################*/ 
    n_index_files = opt2fns(&ndxinfiles,"-n",NFILE,fnm);

    snew(blocka,n_index_files);      //blocka contain the info of the index files
    // blocka[k]->nr     ------> nr. of groups in the given index file
                                 //[int *ngrps]
    // blocka[k]->index  ------> Provide "a" indexes pointing to the first atom
                                 //of each group [atom_id  **index]
    // blocka[k]->nra    ------> Total numebr of atoms in the index file. They 
                                 //[int *nra]
    // blocka[k]->a      ------> Index of each atom that appears in the index file
                                 //in order appearance. [atom_id  **a]
                                 //These numbers are the same that the ones in ndx. 

    snew(n_at_grps,n_index_files);   //Number of atoms in each CM_group

    for (k=0; k< n_index_files;k++) {
        blocka[k] = init_index(ndxinfiles[k],&grpname);

        //Calculation of the number of atoms in each CM_group
        snew(n_at_grps[k],(blocka[k]->nr)+1);  
        for (i=0; i < blocka[k]->nr; i++) {
            n_at_grps[k][i]=blocka[k]->index[i+1]-blocka[k]->index[i];
        }
        ntotalgrps+=blocka[k]->nr;
    }

    if (bDebug) {
        for (k=0; k< n_index_files;k++) {
            print_types(blocka[k]->index, blocka[k]->a, blocka[k]->nr, grpname,&top);
            for (i=0; i < blocka[k]->nr; i++) {
                fprintf(stderr,"i=%u conte %u elements\n",i, n_at_grps[k][i]);
            }
            fprintf(stderr,"Number Atoms=%u and Groups=%u\n",
                    blocka[k]->nra,blocka[k]->nr);
        }
    }


    /*###################################################################
     * Open trx input file for reading 
     *###################################################################*/
    in_file=opt2fn("-f",NFILE,fnm);
    bHaveFirstFrame = read_first_frame(oenv,&status,in_file,&fr,TRX_READ_X);
    if (fr.bPrec)
        fprintf(stderr,"\nPrecision of %s is %g (nm)\n",in_file,1/fr.prec);

    /*####################################################################
     * Preparation of output files
     *####################################################################*/
    
    /* Determine output type and open output for writing */
    out_file=opt2fn("-o",NFILE,fnm);
    ftp=fn2ftp(out_file);
    out=ffopen(out_file, "w");
    //  if (ftp != efGRO) {
    //    gmx_fatal(FARGS,"The current version only allow gro output format");
    //  }
    fprintf(stderr,"\nWill write %s: %s\n",ftp2ext(ftp),ftp2desc(ftp));
    bNeedPrec = (ftp==efGRO || ftp==efXTC);
    if (bNeedPrec) {
        if (bSetPrec || !fr.bPrec)
            fprintf(stderr,"Setting output precision to %g (nm)\n",1/prec);
        else
            fprintf(stderr,"Using output precision of %g (nm)\n",1/prec);
    }

    /* Prepare output trxframe struct */
    if (bNeedPrec && (bSetPrec || !fr.bPrec)) {
        frout.bPrec = TRUE;
        frout.prec  = prec;
    }
     
    /*###################
     * GRO and PDB      #
     *##################*/
    /* Make atoms struct for output in GRO */
    init_t_atoms(&CMatoms,ntotalgrps,FALSE);
    //fprintf(stderr,"CMatoms.nr=%i\n",CMatoms.nr);
    
    /* Setting residue information */ 
    CMatoms.nres=ntotalgrps; //set the number of residues such all CM_groups
                                //in the sabe ndx file belong to the same residue
    //fprintf(stderr,"CMatoms.nres=%i\n",CMatoms.nres);
    snew(CMatoms.resinfo, ntotalgrps); /* t_resinfo Array of residue names and numbers */
    count = 0;
    for ( k = 0; k<n_index_files;k++) {
        for (i=0; i < blocka[k]->nr; i++) {
            CMatoms.resinfo[count].nr = count+1;
            snew(CMatoms.resinfo[count].name,1);
            snew(*CMatoms.resinfo[count].name,10);
            sprintf(*CMatoms.resinfo[count].name, "CM%i", k );
            //fprintf(stderr,"CMatoms.resinfo[%i].nr=%i\n",count, CMatoms.resinfo[count].nr);
            //fprintf(stderr,"CMatoms.resinfo[%i].name=%s\n",k, *CMatoms.resinfo[count].name);
            count++;
        }
    }

    /* Populate other CMatoms structure properies */
    snew(CMatoms.atomname, CMatoms.nr);
    for (k=0; k<CMatoms.nr; k++) {
            /* Set atomname */
            snew (CMatoms.atomname[k],1);
            snew (CMatoms.atomname[k][0],10);
            sprintf(CMatoms.atomname[k][0],"a%s",*CMatoms.resinfo[k].name);
            //fprintf(stderr, "CMatom=%i Atomname=%s\n", count, CMatoms.atomname[count][0]);
            /* Set residex pointing to resinfo */
            CMatoms.atom[k].resind = k;
    }
    
    /*####################################################################
     * Main loop over frames 
     *####################################################################*/
    fprintf(stderr,"\n");
    do {
        snew(xmem,ntotalgrps);
        int suma_at=0;
        for (k=0; k< n_index_files;k++) {  
        /*loop sobre els diferents grups a calcular centre de masses*/
            for (j=0; j < blocka[k]->nr; j++) {
                /* busquem el CM del Fragment*/
                isize_cm=n_at_grps[k][j];
                if (bDebug) {         //debug output
                    fprintf(stderr,
                            "k=%i,ngrups[%i]=%i,n_at_grps[%i][%i]=%i\n",
                             k,k,blocka[k]->nr,k,j,isize_cm);
                }
                snew(index_cm,isize_cm);
                for (i=0;(i < isize_cm);i++){
                    index_cm[i]=blocka[k]->a[blocka[k]->index[j]+i];
                    if (bDebug) {         //debug output
                        atom_type = *(top.atoms.atomname[index_cm[i]]); 
                        fprintf(stderr, "j=%u, i=%i,index_cm[%u]=%u,atom_type=%s\n",
                                       j,i,i,index_cm[i],atom_type);
                    }
                }
                /*Calculem centre massa fragment */
                CM_fragment(isize_cm,index_cm,top,fr,rCM);
                copy_rvec(rCM,xmem[suma_at]); //aqui hi ha el meollo
                suma_at=suma_at+1;
                /*alliberem memoria*/
                sfree(index_cm);
            } 
        }
        /* Volquem la informació en el gro final */
        frout.x=xmem;
        copy_mat(fr.box,frout.box);
        sprintf(title,"Generated by trj_CM : %s t= %9.5f",
		        top_title,fr.time);
        write_hconf_p(out,title,&CMatoms,3,frout.x,NULL,frout.box);
        sfree(xmem);    

    } while(read_next_frame(oenv,status,&fr));

    /*####################################################################
     * Close files and free memory
     *####################################################################*/
    fclose(out);

    return 0;
}
